import { Product } from '@/Models/product';
import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ProductService } from '@services/product.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss']
})
export class CreateProductComponent implements OnInit {

  productId: string = '';
  productForm = new FormGroup({
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    price: new FormControl('', Validators.required),
    image: new FormControl('')
  });

  constructor(
    private toastr: ToastrService,
    private productService: ProductService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  async ngOnInit(): Promise<void> {
    this.productId = this.activatedRoute.snapshot.paramMap.get('id');
    
    if(this.productId){
      var product = (await this.productService.getProductById(this.productId).toPromise());
    
      this.productForm = new FormGroup({
        name: new FormControl(product.name, Validators.required),
        description: new FormControl(product.description, Validators.required),
        price: new FormControl(product.price, Validators.required),
        image: new FormControl(product.image)
      });
    }
  }

  onSubmit(){
    let product: Product = {
      name: this.productForm.value.name,
      price: this.productForm.value.price,
      description: this.productForm.value.description,
      image: this.productForm.value.image
    }

    if(!this.productId) {
    this.productService.createProduct(product).toPromise()
      .then(response => {
        this.router.navigate(['products']);
      })
      .catch(e => {
        this.toastr.error(e);
      });
    } else{
      product.id = parseInt(this.productId);
      this.productService.updateProduct(product).toPromise()
      .then(response => {
        this.router.navigate(['products']);
      })
      .catch(e => {
        this.toastr.error(e);
      });
    }
  }

  getBase64(event) {
   if(event.target.files){
     let reader = new FileReader();
     reader.onload = (e: any) => {
       this.productForm.value.image = e.target.result;
     };
     reader.readAsDataURL(event.target.files[0]);
   }
 }

}
