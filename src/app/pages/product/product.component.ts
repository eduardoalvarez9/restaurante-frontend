import { Product } from '@/Models/product';
import { Component, OnInit } from '@angular/core';
import { ProductService } from '@services/product.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  page = 1;
  pageSize = 4;
  collectionSize = 0;
  products: Product[];

  constructor(private productService: ProductService, private toastrService: ToastrService) { }

  async ngOnInit(): Promise<void> {
    await this.refreshProducts();
  }

  async refreshProducts(){
    let productsResponse = (await this.productService.getProducts().toPromise());
    this.products = productsResponse.$values
      .map((country, i) => ({id: i + 1, ...country}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
    this.collectionSize = productsResponse.$values.length;
  }

  async deleteProduct(id: any){
    await this.productService.deleteProduct(id).toPromise().then(async resp => {
      await this.refreshProducts();
    })
    .catch(error => {
      this.toastrService.error(error);
    })
  }
}
