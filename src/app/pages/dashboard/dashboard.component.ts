import {Component, OnInit} from '@angular/core';
import { OrderService } from '@services/order.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit{
    orders: any;

    constructor(private orderService: OrderService) {
    }

    async ngOnInit(): Promise<void> {
        this.orders = (await this.orderService.getOrders().toPromise()).$values;
        console.log(this.orders);
     }

     isForDelivery(isDelivery: boolean){
         if(isDelivery){
             return 'Si';
         }
         else{
             return 'No';
         }
     }
}
