import { CartProduct } from '@/Models/cart-product';
import { Product } from '@/Models/product';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ProductService } from '@services/product.service';

@Component({
  selector: 'app-cart-step',
  templateUrl: './cart-step.component.html',
  styleUrls: ['./cart-step.component.scss']
})
export class CartStepComponent implements OnInit {

  @Output() productEmitter = new EventEmitter<Array<CartProduct>>();

  products: Product[];
  total = 0;
  cartProducts: Array<CartProduct> = new Array<CartProduct>();
  productFilter = '';
  page = 1;
  pageSize = 5;
  collectionSize = 0;

  constructor(private productService: ProductService) { }

  async ngOnInit(): Promise<void> {
    await this.refreshProducts();
  }

  async refreshProducts(){
    let productsResponse = (await this.productService.getProducts(this.productFilter).toPromise());
    this.products = productsResponse.$values
      .map((country, i) => ({id: i + 1, ...country}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
    this.collectionSize = productsResponse.$values.length;
  }

  selectProduct(product: Product){
    if(this.cartProducts.filter(s => s.productId == product.id).length > 0){
      let index =this.cartProducts.findIndex(s => s.productId == product.id);
      this.cartProducts[index].quantity += 1;
      this.cartProducts[index].total = (this.cartProducts[index].quantity * this.cartProducts[index].price)
    }else{
      this.cartProducts.push(new CartProduct(
        {
          productId: product.id,
          name: product.name,
          price: product.price,
          quantity: 1,
        }));
    }
    this.total = this.cartProducts.reduce((a,b) =>{
      return a + b.total;
    }, 0);

    this.productEmitter.emit(this.cartProducts);
  }

  deleteProduct(index: any){
    this.cartProducts.splice(index,1);
    this.total = this.cartProducts.reduce((a,b) =>{
      return a + b.total;
    }, 0);
    this.productEmitter.emit(this.cartProducts);
  }

}
