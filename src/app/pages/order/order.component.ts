import { CartProduct } from '@/Models/cart-product';
import { Component, OnInit } from '@angular/core';
import Stepper from 'bs-stepper';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  private stepper: Stepper;
  public orderInfo = {};
  public cartProducts: Array<CartProduct> = new Array<CartProduct>();
  constructor() { }

  ngOnInit(): void {
    this.stepper = new Stepper(document.querySelector('#stepper1'), {
      linear: false,
      animation: true
    })
  }

  nextOrdeInfo(info: any) {
    this.orderInfo = info;
    this.stepper.next();
  }

  getCartProduct(products: any){
    this.cartProducts = products;
  }

  next() {
    this.stepper.next();
  }

  onSubmit() {
    return false;
  }

}
