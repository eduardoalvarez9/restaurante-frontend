import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EmployeeService } from '@services/employee.service';

@Component({
  selector: 'order-info-step',
  templateUrl: './order-info-step.component.html',
  styleUrls: ['./order-info-step.component.scss']
})
export class OrderInfoStepComponent implements OnInit {

  @Output() orderInfoEmitter = new EventEmitter<any>();

  orderInfoForm = new FormGroup({
    waiterId: new FormControl('', Validators.required),
    tableId: new FormControl('', Validators.required),
    customerName: new FormControl('', Validators.required),
    isForDelivery: new FormControl(false, Validators.required),
  });

  waiters = [];

  constructor(private employeeService: EmployeeService) { }

  async ngOnInit(): Promise<void> {
    this.waiters = (await this.employeeService.getWaiters().toPromise()).$values;
  }

  changeIsForDelivery(){
    if(this.orderInfoForm.value.isForDelivery){
      this.orderInfoForm.controls['tableId'].setValue(1);
      console.log(this.orderInfoForm);
    }
  }

  onSubmit(){
    this.orderInfoEmitter.emit(this.orderInfoForm.value);
  }
}
