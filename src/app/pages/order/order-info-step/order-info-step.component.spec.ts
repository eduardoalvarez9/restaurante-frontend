import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderInfoStepComponent } from './order-info-step.component';

describe('OrderInfoStepComponent', () => {
  let component: OrderInfoStepComponent;
  let fixture: ComponentFixture<OrderInfoStepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderInfoStepComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderInfoStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
