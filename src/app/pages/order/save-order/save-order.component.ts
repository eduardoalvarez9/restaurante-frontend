import { CartProduct } from '@/Models/cart-product';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrderService } from '@services/order.service';

@Component({
  selector: 'app-save-order',
  templateUrl: './save-order.component.html',
  styleUrls: ['./save-order.component.scss']
})
export class SaveOrderComponent implements OnInit {

  @Input() cartProducts = new Array<CartProduct>();
  @Input() orderInfo: any;
  
  constructor(private orderService: OrderService, 
    private router: Router) { }

  ngOnInit(): void {
  }

  isForDelivery(){
    if(this.orderInfo.isForDelivery){
      return 'Si'
    }
    else{
      return 'No'
    }
  }

  getTotal(){
    return  this.cartProducts.reduce((a,b) =>{
      return a + b.total;
    }, 0);
  }

  async createOrder(){
    let orderRequest = {
      customerName: this.orderInfo.customerName,
      waiterId: this.orderInfo.waiterId,
      tableId: this.orderInfo.tableId,
      isForDelivery: this.orderInfo.isForDelivery,
      orderDetails: this.cartProducts.map(product => {
        return {
          productId: product.productId,
          quantity: product.quantity
        }
      })
    };

    await this.orderService.createOrder(orderRequest).toPromise()
      .then(() => this.router.navigate(['']))
      .catch(error => console.log(error));
  }

}
