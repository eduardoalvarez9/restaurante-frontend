import { Employee } from '@/Models/employee';
import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '@services/employee.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {

  page = 1;
  pageSize = 4;
  collectionSize = 0;
  employees: Employee[];

  constructor(
    private toastrService: ToastrService,
    private employeeService: EmployeeService
  ) { }

  async ngOnInit(): Promise<void> {
    await this.refreshEmployees();
  }

  async refreshEmployees(){
    let employeesResponse = (await this.employeeService.getEmployees().toPromise());
    this.employees = employeesResponse.$values
      .map((country, i) => ({id: i + 1, ...country}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
    this.collectionSize = employeesResponse.$values.length;
  }

  async deleteEmployee(id: any){
    await this.employeeService.deleteEmployee(id).toPromise().then(async resp => {
      await this.refreshEmployees();
    })
    .catch(error => {
      this.toastrService.error(error);
    })
  }

}
