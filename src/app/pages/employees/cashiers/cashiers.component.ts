import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '@services/employee.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cashiers',
  templateUrl: './cashiers.component.html',
  styleUrls: ['./cashiers.component.scss']
})
export class CashiersComponent implements OnInit {

  employees: [{
    id?: number;
    idNumber: number;
    names: string;
    lastNames: string;
    selected: boolean;
  }];

  cashiers = [];

  constructor(
    private toastrService: ToastrService,
    private employeeService: EmployeeService
  ) { }

  async ngOnInit(): Promise<void> {
    await this.refresh();
  }

  async refresh(){
    this.employees = (await this.employeeService.getPotentialCashiers().toPromise()).$values.map(m => {
      return {
        id: m.id,
        idNumber: m.idNumber,
        names: m.names,
        lastNames: m.lastNames
      }});
    
      this.cashiers = (await this.employeeService.getCashiers().toPromise()).$values.map(m => {
        return {
          id: m.id,
          idNumber: m.employee.idNumber,
          names: m.employee.names,
          lastNames: m.employee.lastNames
        }
      });
  }

  async asignCashiers(){
    var ids = this.employees.filter(s => s.selected).map(m => m.id);
    await this.employeeService.asignCashiers(ids).toPromise().then(s => this.refresh())
    .catch(e => this.toastrService.error(e));
  }

  async unasignCashiers(){
    var ids = this.cashiers.filter(s => s.selected).map(m => m.id);
    await this.employeeService.unasignCashiers(ids).toPromise().then(s => this.refresh())
    .catch(e => this.toastrService.error(e));
  }
}
