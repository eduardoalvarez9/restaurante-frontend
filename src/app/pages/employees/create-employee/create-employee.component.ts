import { Employee } from '@/Models/employee';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from '@services/employee.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.scss']
})
export class CreateEmployeeComponent implements OnInit {

  employeeId: string = '';
  employeeForm = new FormGroup({
    names: new FormControl('', Validators.required),
    lastNames: new FormControl('', Validators.required),
    idNumber: new FormControl('', Validators.required),
  });
  
  constructor(
    private toastr: ToastrService,
    private employeeService: EmployeeService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  async ngOnInit(): Promise<void> {
    this.employeeId = this.activatedRoute.snapshot.paramMap.get('id');
    if(this.employeeId){
      let employee = (await this.employeeService.getEmployeeById(this.employeeId).toPromise())
      this.employeeForm = new FormGroup({
        names: new FormControl(employee.names, Validators.required),
        lastNames: new FormControl(employee.lastNames, Validators.required),
        idNumber: new FormControl(employee.idNumber, Validators.required),
      });
    }
  }

  onSubmit(){
    let employee: Employee = {
      names: this.employeeForm.value.names,
      idNumber: this.employeeForm.value.idNumber,
      lastNames: this.employeeForm.value.lastNames
    }

    if(!this.employeeId) {
      employee.id = null;
    this.employeeService.createEmployee(employee).toPromise()
      .then(response => {
        this.router.navigate(['employees']);
      })
      .catch(e => {
        this.toastr.error(e);
      });
    } else{
      employee.id = parseInt(this.employeeId);
      this.employeeService.updateEmployee(employee).toPromise()
      .then(response => {
        this.router.navigate(['employees']);
      })
      .catch(e => {
        this.toastr.error(e);
      });
    }
  }

}
