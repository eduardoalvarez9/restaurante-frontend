import { Employee } from '@/Models/employee';
import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '@services/employee.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-waiters',
  templateUrl: './waiters.component.html',
  styleUrls: ['./waiters.component.scss']
})
export class WaitersComponent implements OnInit {

  employees: [{
    id?: number;
    idNumber: number;
    names: string;
    lastNames: string;
    selected: boolean;
  }];

  waiters = [];
  
  constructor(
    private toastrService: ToastrService,
    private employeeService: EmployeeService
  ) { }

  async ngOnInit(): Promise<void> {
    await this.refresh();
  }

  async refresh(){
    this.employees = (await this.employeeService.getPotentialWaiters().toPromise()).$values.map(m => {
      return {
        id: m.id,
        idNumber: m.idNumber,
        names: m.names,
        lastNames: m.lastNames
      }});
    
      this.waiters = (await this.employeeService.getWaiters().toPromise()).$values.map(m => {
        return {
          id: m.id,
          idNumber: m.employee.idNumber,
          names: m.employee.names,
          lastNames: m.employee.lastNames
        }
      });
  }

  async asignWaiters(){
    var ids = this.employees.filter(s => s.selected).map(m => m.id);
    await this.employeeService.asignWaiters(ids).toPromise().then(s => this.refresh())
    .catch(e => this.toastrService.error(e));
  }

  async unasignWaiters(){
    var ids = this.waiters.filter(s => s.selected).map(m => m.id);
    await this.employeeService.unasignWaiters(ids).toPromise().then(s => this.refresh())
    .catch(e => this.toastrService.error(e));
  }

}
