import {Component, OnInit} from '@angular/core';
import { ProductService } from '@services/product.service';
@Component({
    selector: 'app-blank',
    templateUrl: './blank.component.html',
    styleUrls: ['./blank.component.scss']
})
export class BlankComponent implements OnInit {
    
    constructor(private productService: ProductService) {
    }

    async ngOnInit(){
        // let products = (await this.productService.getProducts().toPromise());
        // console.log(products);
    }
}
