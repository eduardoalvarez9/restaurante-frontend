import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MainComponent} from '@modules/main/main.component';
import {BlankComponent} from '@pages/blank/blank.component';
import {LoginComponent} from '@modules/login/login.component';
import {ProfileComponent} from '@pages/profile/profile.component';
import {RegisterComponent} from '@modules/register/register.component';
import {DashboardComponent} from '@pages/dashboard/dashboard.component';
import {AuthGuard} from '@guards/auth.guard';
import {NonAuthGuard} from '@guards/non-auth.guard';
import {ForgotPasswordComponent} from '@modules/forgot-password/forgot-password.component';
import {RecoverPasswordComponent} from '@modules/recover-password/recover-password.component';
import {PrivacyPolicyComponent} from '@modules/privacy-policy/privacy-policy.component';
import { ProductComponent } from '@pages/product/product.component';
import { CreateProductComponent } from '@pages/product/create-product/create-product.component';
import { EmployeesComponent } from '@pages/employees/employees.component';
import { CreateEmployeeComponent } from '@pages/employees/create-employee/create-employee.component';
import { CashiersComponent } from '@pages/employees/cashiers/cashiers.component';
import { WaitersComponent } from '@pages/employees/waiters/waiters.component';
import { OrderComponent } from '@pages/order/order.component';
import { OrderDetailsComponent } from './dashboard/order-details/order-details.component';

const routes: Routes = [
    {
        path: '',
        component: MainComponent,
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        children: [
            {
                path: 'order',
                component: OrderComponent,
            },
            {
                path: 'products',
                component: ProductComponent,
            },
            {
                path: 'addProduct',
                component: CreateProductComponent
            },
            {
                path: 'addProduct/:id',
                component: CreateProductComponent
            },
            {
                path: 'employees',
                component: EmployeesComponent,
            },
            {
                path: 'createEmployee',
                component: CreateEmployeeComponent,
            },
            {
                path: 'createEmployee/:id',
                component: CreateEmployeeComponent,
            },
            {
                path: 'cashiers',
                component: CashiersComponent 
            },
            {
                path: 'waiters',
                component: WaitersComponent
            },
            {
                path: 'orderDetails/:id',
                component: OrderDetailsComponent
            },
            {
                path: '',
                component: DashboardComponent
            }
        ]
    },
    {
        path: 'login',
        component: LoginComponent,
        canActivate: [NonAuthGuard]
    },
    {
        path: 'register',
        component: RegisterComponent,
        canActivate: [NonAuthGuard]
    },
    {
        path: 'forgot-password',
        component: ForgotPasswordComponent,
        canActivate: [NonAuthGuard]
    },
    {
        path: 'recover-password',
        component: RecoverPasswordComponent,
        canActivate: [NonAuthGuard]
    },
    {
        path: 'privacy-policy',
        component: PrivacyPolicyComponent,
        canActivate: [NonAuthGuard]
    },
    {path: '**', redirectTo: ''}
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {relativeLinkResolution: 'legacy'})],
    exports: [RouterModule]
})
export class AppRoutingModule {}
