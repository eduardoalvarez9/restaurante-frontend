import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from '@services/order.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {

  orderId = "";
  order: any;
  constructor(private activatedRoute: ActivatedRoute, private orderService: OrderService) {
    this.orderId = this.activatedRoute.snapshot.paramMap.get('id');
    
   }

  async ngOnInit(): Promise<void> {
    this.order = (await this.orderService.getOrder(this.orderId).toPromise());
    console.log(this.order);
  }

  isForDelivery(){
    if(this.order.isForDelivery){
      return 'Si'
    }
    else{
      return 'No'
    }
  }

  getTotal(){
    return  this.order.orderDetails.$values.reduce((a,b) =>{
      return a + (b.quantity * b.product.price);
    }, 0);
  }

}
