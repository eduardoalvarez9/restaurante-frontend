import { ApiResponse } from '@/Models/api-response';
import { Product } from '@/Models/product';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private url = 'http://localhost:5000/api/product'

  constructor(private httpClient: HttpClient) { }

  getProducts(filter?: string): Observable<ApiResponse<Product>>{
    if(filter){
      return this.httpClient.get<ApiResponse<Product>>(`${this.url}/AllProducts/${filter}`);
    }
    return this.httpClient.get<ApiResponse<Product>>(`${this.url}/AllProducts`);
  }

  getProductById(id: string): Observable<Product>{
    return this.httpClient.get<Product>(`${this.url}/GetById/${id}`);
  }

  createProduct(product: Product){
    return this.httpClient.post(`${this.url}`, product);
  }

  updateProduct(product: Product){
    return this.httpClient.put(`${this.url}/${product.id}`, product);
  }

  deleteProduct(id: string){
    return this.httpClient.delete(`${this.url}/${id}`);
  }

}
