import { ApiResponse } from '@/Models/api-response';
import { Employee } from '@/Models/employee';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private url = 'http://localhost:5000/api/Employee'

  constructor(private httpClient: HttpClient) { }

  getEmployees(): Observable<ApiResponse<Employee>>{
    return this.httpClient.get<ApiResponse<Employee>>(`${this.url}/AllEmployees`);
  }

  getPotentialWaiters(): Observable<ApiResponse<Employee>>{
    return this.httpClient.get<ApiResponse<Employee>>(`${this.url}/GetPotentialWaiters`);
  }

  getWaiters(): Observable<ApiResponse<any>>{
    return this.httpClient.get<ApiResponse<any>>(`${this.url}/GetWaiters`);
  }

  getPotentialCashiers(): Observable<ApiResponse<Employee>>{
    return this.httpClient.get<ApiResponse<Employee>>(`${this.url}/GetPotentialCashiers`);
  }

  getCashiers(): Observable<ApiResponse<any>>{
    return this.httpClient.get<ApiResponse<any>>(`${this.url}/GetCashiers`);
  }

  getEmployeeById(id: string): Observable<Employee>{
    return this.httpClient.get<Employee>(`${this.url}/GetById/${id}`);
  }

  createEmployee(employee: Employee){
    return this.httpClient.post(`${this.url}`, employee);
  }

  asignWaiters(ids: Array<number>){
    return this.httpClient.post(`${this.url}/AsignWaiters`, ids);
  }

  unasignWaiters(ids: Array<number>){
    return this.httpClient.post(`${this.url}/UnasignWaiters`, ids);
  }

  asignCashiers(ids: Array<number>){
    return this.httpClient.post(`${this.url}/AsignCashiers`, ids);
  }

  unasignCashiers(ids: Array<number>){
    return this.httpClient.post(`${this.url}/UnasignCashiers`, ids);
  }

  updateEmployee(employee: Employee){
    return this.httpClient.put(`${this.url}/${employee.id}`, employee);
  }

  deleteEmployee(id: string){
    return this.httpClient.delete(`${this.url}/${id}`);
  }
}
