import { LoginResponse } from '@/Models/login-response';
import { HttpClient } from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import jwt_decode from 'jwt-decode';

@Injectable({
    providedIn: 'root'
})

export class AppService {
    
    private url = 'http://localhost:5000/api'
    public user: any = null;
    
    constructor(
        private router: Router,
        private toastr: ToastrService,
        private httpClient: HttpClient) {}

    async loginByAuth({email, password}) {
        try {
            let response = (await this.httpClient.post<LoginResponse>(`${this.url}/Auth/login`, {userName: email, password}).toPromise());
            const token = response.token ;
            localStorage.setItem('token', token);
            await this.getProfile();
            this.router.navigate(['/']);
        } catch (error) {
            this.toastr.error(error.message);
        }
    }

    async registerByAuth({email, password}) {
        try {
            const token = "";//await Gatekeeper.registerByAuth(email, password);
            localStorage.setItem('token', token);
            await this.getProfile();
            this.router.navigate(['/']);
        } catch (error) {
            this.toastr.error(error.response.data.message);
        }
    }

    
    async getProfile() {
        try {
          this.user = jwt_decode(localStorage.getItem('token'));
        } catch (error) {
            this.logout();
            throw error;
        }
    }

    logout() {
        localStorage.removeItem('token');
        this.user = null;
        this.router.navigate(['/login']);
    }
}
