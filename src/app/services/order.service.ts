import { ApiResponse } from '@/Models/api-response';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private url = 'http://localhost:5000/api/order'
  
  constructor(private httpClient: HttpClient) { }

  createOrder(orderRequest: any){
    return this.httpClient.post(`${this.url}/Create`, orderRequest);
  }

  getOrders(){
    return this.httpClient.get<any>(`${this.url}`);
  }

  getOrder(id: string){
    return this.httpClient.get<any>(`${this.url}/details/${id}`);
  }
}
