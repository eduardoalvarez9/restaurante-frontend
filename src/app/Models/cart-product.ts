export class CartProduct {
    productId: number;
    quantity: number;
    name: string;
    price: number;
    total?: number;

    constructor(product: CartProduct) {
      this.productId = product.productId;
      this.quantity = product.quantity;
      this.name = product.name;
      this.price = product.price;
      this.total = (product.price * product.quantity);
    }
}