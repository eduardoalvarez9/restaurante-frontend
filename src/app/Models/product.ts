export class Product {
    id?: number
    name: string;
    price: number;
    description: string;
    image: string;

    constructor() {
        this.id = null;
        this.name = '';
        this.price = 0;
        this.description = '';
        this.image = '';
    }
}