export class ApiResponse<T> {
    [x: string]: any;
    id: number;
    values: Array<T>;
}